<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public $guarded = ['id'];


    public function students()
    {
        return $this->belongsToMany(Student::class, 'exam_student');
    }

    public function result()
    {
        return $this->hasOne(Result::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }
}
