<?php

namespace App\Repository\Contract;


class BaseRepository
{
    protected $model;

    public function all(array $relation = [])
    {
        if (!empty($relation)) {
            return $this->model::with($relation)->all()->get();
        }
        return $this->model::all();
    }

    public function find(int $id, array $relation = [])
    {
        if (!empty($relation)) {
            return $this->model::with($relation)->find($id);
        }
        return $this->model::find($id);
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function delete(int $id)
    {
        $item = $this->find($id);
        return $item::delete();
    }

    public function update(int $id, int $data)
    {
        $item = $this->find($id);
        return $item::update($data);
    }

    public function findBy(array $criteria, $relations = [], $single = true)
    {
        $query = $this->model::query();
        if (!is_null($relations)) {
            $query = $this->model::with($relations);
        }
        foreach ($criteria as $key => $value) {
            $query->where($key, $value);
        }
        if ($single) {
            return $query->first();
        }
        return $query->get();
    }


}